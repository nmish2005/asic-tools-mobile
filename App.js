'use strict';

import React from 'react';
import Ripple from 'react-native-material-ripple';
import {AppRegistry, ScrollView, TouchableOpacity, Text, View} from 'react-native';
import mainStyles from './assets/styles/main';
import {Path, Svg} from 'react-native-svg';
import Translations from './assets/Translations';

import Account from "./src/Account";
import Dashboard from "./src/Dashboard";
import LoginForm from "./src/_LoginForm";

const currentYear = new Date().getFullYear();

export default class App extends React.Component {
    constructor() {
        super();
        this.state = {
            isUser: false,
            blueMenuToggled: false,
            locale: 'RU',
            nextLocale: 'EN',
            view: Account
        };

        this.toggleNav = this.toggleNav.bind(this);
        this.setLocale = this.setLocale.bind(this);
        this.goToAccount = this.goToAccount.bind(this);
        this.goToDashboard = this.goToDashboard.bind(this);
        this.logOut = this.logOut.bind(this);
        this.translate = this.translate.bind(this);
        this.handleAuth = this.handleAuth.bind(this);
        this.checkIfLoggedIn = this.checkIfLoggedIn.bind(this);
        this.loginAsUser = this.loginAsUser.bind(this);
    }

    translate(word) {
        return Translations[word][this.state.locale];
    }

    handleAuth() {
        this.setState({isUser: true});
    }

    goToAccount() {
        this.setState({view: Account, blueMenuToggled: false});

        mainStyles.hamburger.paddingTop = 30;
    }

    goToDashboard() {
        this.setState({view: Dashboard, blueMenuToggled: false});

        mainStyles.hamburger.paddingTop = 30;
    }

    logOut() {
        this.setState({isUser: false, blueMenuToggled: false});
        mainStyles.hamburger.paddingTop = 30;

        this.request('https://bitrex.vn.ua/account/logout', 'text/plain', '');
    }

    toggleNav() {
        if (this.state.blueMenuToggled)
            mainStyles.hamburger.paddingTop = 30;
        else
            mainStyles.hamburger.paddingTop = 165;

        this.setState({blueMenuToggled: !this.state.blueMenuToggled});
    }

    setLocale() {
        const locale = this.state.nextLocale,
            nextLocale = this.state.locale;

        this.setState({locale, nextLocale});
    }

    request(url: String, ct: String, data: String, callback: Function) {
        const xhr = new XMLHttpRequest;

        xhr.open('POST', url, true);

        xhr.setRequestHeader("Content-Type", ct);

        xhr.send(data);

        xhr.onload = callback;
    }

    checkIfLoggedIn(callback: Function) {
        this.request('https://bitrex.vn.ua/ajax/getCustomerDevicesData', 'text/plain', '', (data) => {
            return data.target.responseText === 'Access denied' ? callback(false) : callback(true);
        });
    }

    loginAsUser(){
        this.request('https://bitrex.vn.ua/ajax/signin', 'text/plain', 'login=vindiamond0%40gmail.com&pass=snickers128&lang=ru', () => {});
    }

    render() {
        this.checkIfLoggedIn(bool => !bool && this.loginAsUser());

        return (
            <View style={mainStyles.MainView}>
                <TouchableOpacity onPress={this.toggleNav} style={mainStyles.hamburger}>
                    {/*Image: hamburger*/}
                    <Svg xmlns="http://www.w3.org/2000/svg" width="30" height="30"
                         viewBox="0 0 30 30"
                         fill='#000'>
                        <Path
                            d="M 3 7 A 1.0001 1.0001 0 1 0 3 9 L 27 9 A 1.0001 1.0001 0 1 0 27 7 L 3 7 z M 3 14 A 1.0001 1.0001 0 1 0 3 16 L 27 16 A 1.0001 1.0001 0 1 0 27 14 L 3 14 z M 3 21 A 1.0001 1.0001 0 1 0 3 23 L 27 23 A 1.0001 1.0001 0 1 0 27 21 L 3 21 z"/>
                    </Svg>
                </TouchableOpacity>
                {this.state.blueMenuToggled &&
                <View style={mainStyles.blueNav}>
                    <Ripple onPress={this.goToDashboard}
                                      style={{flexDirection: 'row', paddingTop: 15}}>
                        {/*Image: monitoring*/}
                        <Svg xmlns="http://www.w3.org/2000/svg"
                             style={{width: 22, height: 27, marginLeft: 15}}
                             viewBox="0 0 512 512">
                            <Path fill="#fff"
                                  d="M500 384c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12H12c-6.6 0-12-5.4-12-12V76c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v308h436zM372.7 159.5L288 216l-85.3-113.7c-5.1-6.8-15.5-6.3-19.9 1L96 248v104h384l-89.9-187.8c-3.2-6.5-11.4-8.7-17.4-4.7z"/>
                        </Svg>
                        <Text style={{
                            fontWeight: '500',
                            fontSize: 17,
                            color: '#fff',
                            paddingLeft: 15
                        }}>{this.translate('dashboard')}</Text>
                    </Ripple>
                    <Ripple onPress={this.goToAccount}
                                      style={{flexDirection: 'row', paddingTop: 15}}>
                        {/*Image: account*/}
                        <Svg xmlns="http://www.w3.org/2000/svg"
                             style={{width: 22, height: 27, marginLeft: 15}}
                             viewBox="0 0 512 512">
                            <Path fill="#fff"
                                  d="M256 0c88.366 0 160 71.634 160 160s-71.634 160-160 160S96 248.366 96 160 167.634 0 256 0zm183.283 333.821l-71.313-17.828c-74.923 53.89-165.738 41.864-223.94 0l-71.313 17.828C29.981 344.505 0 382.903 0 426.955V464c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48v-37.045c0-44.052-29.981-82.45-72.717-93.134z"/>
                        </Svg>
                        <Text
                            style={{
                                fontWeight: '500',
                                fontSize: 17,
                                color: '#fff',
                                paddingLeft: 15
                            }}>{this.translate('account')}</Text>
                    </Ripple>
                    <Ripple onPress={this.logOut} style={{flexDirection: 'row', paddingTop: 15}}>
                        <Svg xmlns="http://www.w3.org/2000/svg" style={{width: 22, height: 27, marginLeft: 15}}
                             viewBox="0 0 512 512">
                            <Path fill="#fff"
                                  d="M497 273L329 441c-15 15-41 4.5-41-17v-96H152c-13.3 0-24-10.7-24-24v-96c0-13.3 10.7-24 24-24h136V88c0-21.4 25.9-32 41-17l168 168c9.3 9.4 9.3 24.6 0 34zM192 436v-40c0-6.6-5.4-12-12-12H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h84c6.6 0 12-5.4 12-12V76c0-6.6-5.4-12-12-12H96c-53 0-96 43-96 96v192c0 53 43 96 96 96h84c6.6 0 12-5.4 12-12z"/>
                        </Svg>
                        <Text style={{
                            fontWeight: '500',
                            fontSize: 17,
                            color: '#fff',
                            paddingLeft: 15
                        }}>{this.translate('logout')}</Text>
                    </Ripple>
                </View>
                }

                <ScrollView>
                    <View style={mainStyles.mainNav}>
                        <Text style={mainStyles.navbarBrand}>ASIC.TOOLS</Text>
                        <View
                            style={{flexDirection: 'row', width: '30%', justifyContent: 'space-around', padding: 15}}>
                            <Ripple onPress={this.setLocale} style={mainStyles.navButtonLocale}>
                                <Text style={{
                                    fontWeight: '900',
                                    color: '#fff',
                                    textAlign: 'center'
                                }}>{this.state.nextLocale}</Text>
                            </Ripple>
                        </View>
                    </View>

                    <this.state.view translateAgent={this.translate} appState={this.state}/>

                    {/*{this.state.isUser &&*/}
                    {/*<this.state.view translateAgent={this.translate} appState={this.state}/>*/}
                    {/*}*/}

                    {/*{!this.state.isUser &&*/}
                    {/*<LoginForm translateAgent={this.translate} appState={this.state} changeLocale={this.setLocale}*/}
                               {/*onAuthChange={this.handleAuth}/>*/}
                    {/*}*/}

                    <View style={mainStyles.footer}>
                        <Text style={mainStyles.footerText}>Asic.Tools &copy; {currentYear}</Text>
                    </View>
                </ScrollView>

            </View>

        );
    }
}

AppRegistry.registerComponent('AsicTools', () => App);