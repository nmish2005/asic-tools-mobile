import React from 'react';
import GridView from "react-native-super-grid";
import {Text} from "react-native";
import Ripple from "react-native-material-ripple";

export default class QuietMode extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            hours: [
                {name: 1, active: this.props.hours.includes(1)},
                {name: 2, active: this.props.hours.includes(2)},
                {name: 3, active: this.props.hours.includes(3)},
                {name: 4, active: this.props.hours.includes(4)},
                {name: 5, active: this.props.hours.includes(5)},
                {name: 6, active: this.props.hours.includes(6)},
                {name: 7, active: this.props.hours.includes(7)},
                {name: 8, active: this.props.hours.includes(8)},
                {name: 9, active: this.props.hours.includes(9)},
                {name: 10, active: this.props.hours.includes(10)},
                {name: 11, active: this.props.hours.includes(11)},
                {name: 12, active: this.props.hours.includes(12)},
                {name: 13, active: this.props.hours.includes(13)},
                {name: 14, active: this.props.hours.includes(14)},
                {name: 15, active: this.props.hours.includes(15)},
                {name: 16, active: this.props.hours.includes(16)},
                {name: 17, active: this.props.hours.includes(17)},
                {name: 18, active: this.props.hours.includes(18)},
                {name: 19, active: this.props.hours.includes(19)},
                {name: 20, active: this.props.hours.includes(20)},
                {name: 21, active: this.props.hours.includes(21)},
                {name: 22, active: this.props.hours.includes(22)},
                {name: 23, active: this.props.hours.includes(23)},
                {name: 24, active: this.props.hours.includes(24)}
            ]
        };

        this.toggleHour = this.toggleHour.bind(this);
    }

    toggleHour(data: Object){
        let tmp = this.state.hours;
        delete tmp[data];
        tmp.push(data);

        this.setState({hours: tmp});
    }

    render() {
        const styles = {
                gridView: {
                    paddingTop: 25,
                    flex: 1,
                },
                itemContainer: {
                    borderRadius: 5,
                    padding: 5,
                },
                itemName: {
                    textAlign: 'center',
                    fontWeight: '700'
                }
            };

        let count = -1;

        return (<GridView
                itemDimension={35}
                items={this.state.hours}
                style={styles.gridView}
                renderItem={item => {
                    count++;

                    return (<Ripple key={count} onPress={() => this.toggleHour(item)}
                                    style={[styles.itemContainer, {backgroundColor: item.active ? '#00bcd4' : '#eff0f1'}]}>
                            <Text style={[styles.itemName, {color: item.active ? '#fff' : '#666'}]}>{item.name}</Text>
                        </Ripple>
                    )
                }}
            />
        );
    }
}

