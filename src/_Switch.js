import React from 'react';
import {TouchableOpacity, Animated} from "react-native";

export default class Switch extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            isToggled: props.isToggled
        };
    }

    toggle(){
        this.setState({isToggled: !this.state.isToggled});

        return setTimeout(this.props.onToggle(this.state.isToggled), 1000);
    }

    render(){
        return (
            <TouchableOpacity onPress={this.toggle} style={{backgroundColor: '#00bcd4', borderRadius: 25, width: 35, height: 15}}>
                <Animated.View style={{backgroundColor: 'ghostwhite', borderRadius: 50, width: 17, height: 17, transform: [{translateX: this.state.isToggled ? 19 : -1}]}}/>
            </TouchableOpacity>
        );
    }
}