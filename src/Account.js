import React from 'react';
import {Linking, Picker, ScrollView, Text, TextInput, TouchableOpacity, View} from 'react-native';
import Ripple from 'react-native-material-ripple';
import Modal from 'react-native-modal';
import mainStyles from "../assets/styles/main";
import CheckBox from 'react-native-check-box';
import {Path, Svg} from "react-native-svg";
import QuietMode from "./_QuietMode";

let appState;

class Account extends React.Component {
    constructor(props) {
        super(props);

        appState = props.appState;

        this.email = 'Mike@asic.tools';

        this.state = {
            showTGUnaviableModal: false,
            notificationLocale: appState.locale,
            notificationTiming: 'every3hours',
            emailEnabled: true,
            tgUsers: [
                {name: 'TG user1', quietWidgetToggled: false, enabled: true, hours: [1, 2, 3, 4, 5, 6, 7]},
                {name: 'TG user2', quietWidgetToggled: true, enabled: false, hours: [7, 8, 10, 23]}
            ]
        };
        this.translate = props.translateAgent;

        this.openTelegram = this.openTelegram.bind(this);
    }

    openTelegram() {
        let url = 'tg://resolve?domain=BitrexMonitoringBot';

        Linking.canOpenURL(url).then(supported => {
            if (!supported) {
                this.setState({showTGUnaviableModal: true});
            } else {
                return Linking.openURL(url);
            }
        }).catch(err => this.setState({showTGUnaviableModal: true}));
    }

    render() {
        return (
            <ScrollView style={{padding: 15}}>
                <View style={{flexDirection: 'row', padding: 15}}>
                    {/*Image: account*/}
                    <Svg xmlns="http://www.w3.org/2000/svg"
                         style={{width: 35, height: 40, marginRight: 15}}
                         viewBox="0 0 512 512">
                        <Path fill="#000"
                              d="M256 0c88.366 0 160 71.634 160 160s-71.634 160-160 160S96 248.366 96 160 167.634 0 256 0zm183.283 333.821l-71.313-17.828c-74.923 53.89-165.738 41.864-223.94 0l-71.313 17.828C29.981 344.505 0 382.903 0 426.955V464c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48v-37.045c0-44.052-29.981-82.45-72.717-93.134z"/>
                    </Svg>
                    <Text
                        style={mainStyles.header}>{this.translate('account')}</Text>
                </View>

                <ScrollView style={{padding: 25, backgroundColor: '#fff', margin: 25, marginTop: 0}}>
                    <Text style={mainStyles.subHeader}>{this.translate('contactediting')}</Text>

                    <View>
                        <Text style={{width: '100%'}}>{this.translate('yourName')}</Text>
                        <TextInput underlineColorAndroid={'transparent'} spellCheck={false} style={{
                            width: '100%',
                            borderColor: '#ccc',
                            borderStyle: 'solid',
                            borderWidth: 1
                        }} value={''}/>
                    </View>
                    <View>
                        <Text style={{width: '100%'}}>{this.translate('oldpass')}<Text
                            style={mainStyles.important}>*</Text></Text>
                        <TextInput underlineColorAndroid={'transparent'} spellCheck={false} style={{
                            width: '100%',
                            borderColor: '#ccc',
                            borderStyle: 'solid',
                            borderWidth: 1
                        }} value={''}/>
                    </View>

                    <View>
                        <Text style={{width: '100%'}}>{this.translate('email')}<Text
                            style={mainStyles.important}>*</Text></Text>
                        <TextInput underlineColorAndroid={'transparent'} spellCheck={false} style={{
                            width: '100%',
                            borderColor: '#ccc',
                            borderStyle: 'solid',
                            borderWidth: 1
                        }} value={''}/>
                    </View>
                    <View>
                        <Text style={{width: '100%'}}>{this.translate('newpass')}</Text>
                        <TextInput underlineColorAndroid={'transparent'} secureTextEntry={true} spellCheck={false}
                                   style={{
                                       width: '100%',
                                       borderColor: '#ccc',
                                       borderStyle: 'solid',
                                       borderWidth: 1
                                   }} value={''}/>
                    </View>

                    <View>
                        <Text style={{width: '100%'}}>{this.translate('new2pass')}</Text>
                        <TextInput underlineColorAndroid={'transparent'} spellCheck={false} secureTextEntry={true}
                                   style={{
                                       width: '100%',
                                       borderColor: '#ccc',
                                       borderStyle: 'solid',
                                       borderWidth: 1
                                   }} value={''}/>
                    </View>

                    <View>
                        <Text style={{width: '100%'}}>{this.translate('fballowed')}</Text>
                        <Svg xmlns="http://www.w3.org/2000/svg" style={{width: 35, height: 35}}
                             viewBox="0 0 448 512">
                            <Path fill="#3b5998"
                                  d="M448 56.7v398.5c0 13.7-11.1 24.7-24.7 24.7H309.1V306.5h58.2l8.7-67.6h-67v-43.2c0-19.6 5.4-32.9 33.5-32.9h35.8v-60.5c-6.2-.8-27.4-2.7-52.2-2.7-51.6 0-87 31.5-87 89.4v49.9h-58.4v67.6h58.4V480H24.7C11.1 480 0 468.9 0 455.3V56.7C0 43.1 11.1 32 24.7 32h398.5c13.7 0 24.8 11.1 24.8 24.7z"/>
                        </Svg>
                    </View>

                    <Ripple style={[mainStyles.button, {width: 130, alignItems: 'center'}]}>
                        <View style={{flexDirection: 'row'}}>
                            <Svg style={{width: 20, height: 20, marginRight: 10}} xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 448 512">
                                <Path fill="#fff"
                                      d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z"/>
                            </Svg>
                            <Text style={mainStyles.buttonText}>{this.translate('save')}</Text>
                        </View>
                    </Ripple>
                </ScrollView>

                <ScrollView style={{padding: 25, backgroundColor: '#fff', margin: 25, marginTop: 0}}>
                    <Text style={mainStyles.subHeader}>{this.translate('notifications')}</Text>

                    <View>
                        <View>
                            <Text>{this.translate('notificationslang')}</Text>
                            <Picker mode={'dropdown'}
                                    onValueChange={(lang, index) => this.setState({notificationLocale: lang})}
                                    style={{width: 100}} selectedValue={this.state.notificationLocale}>
                                <Picker.Item label={'RU'} value={'RU'}/>
                                <Picker.Item label={'EN'} value={'EN'}/>
                            </Picker>
                        </View>

                        <View>
                            <Text>{this.translate('notifyme')}</Text>
                            <Picker mode={'dropdown'}
                                    onValueChange={(time, index) => this.setState({notificationTiming: time})}
                                    style={{width: 150}} selectedValue={this.state.notificationTiming}>
                                <Picker.Item label={this.translate('onetime')} value={'onetime'}/>
                                <Picker.Item label={this.translate('everyhour')} value={'everyhour'}/>
                                <Picker.Item label={this.translate('every3hours')} value={'every3hours'}/>
                                <Picker.Item label={this.translate('onceaday')} value={'onceaday'}/>
                            </Picker>
                        </View>

                        <View>
                            <CheckBox
                                style={{flex: 1, padding: 10}}
                                onClick={() => void(0)}
                                isChecked={this.state.emailEnabled}
                                rightText={this.translate('receiveemail') + this.email}
                            />

                            {this.state.tgUsers.map((value, index, arr) =>
                                <View>
                                    <View style={{flexDirection: 'row'}}>
                                        <CheckBox
                                            key={index}
                                            style={{flex: 1, padding: 10}}
                                            onClick={() => void(0)}
                                            isChecked={value.enabled}
                                            rightText={this.translate('receivetg') + value.name}
                                        />
                                        <Text style={mainStyles.link}
                                              onPress={() => this.setState({})}>{this.translate('silentmode')}</Text>
                                    </View>
                                    {value.quietWidgetToggled && <View>
                                        <QuietMode translateAgent={this.translate} hours={value.hours} key={index}
                                                   onSubmit={this.setHours}/>
                                    </View>}
                                </View>
                            )}
                        </View>
                    </View>
                    <View>
                        <Text>{this.translate('canreceive')}</Text>
                        <Text style={{flexDirection: 'row'}}>
                            <Text>{this.translate('botinstructions')}</Text>
                            <Text style={mainStyles.link} onPress={this.openTelegram}>@BitrexMonitoringBot</Text>
                            <Text>{this.translate('botinstructionsp2')}</Text>
                        </Text>
                        <Text>{this.translate('tgallowed')}</Text>
                    </View>

                    <Ripple style={[mainStyles.button, {width: 130, alignItems: 'center'}]}>
                        <View style={{flexDirection: 'row'}}>
                            <Svg style={{width: 20, height: 20, marginRight: 10}} xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 448 512">
                                <Path fill="#fff"
                                      d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM224 416c-35.346 0-64-28.654-64-64 0-35.346 28.654-64 64-64s64 28.654 64 64c0 35.346-28.654 64-64 64zm96-304.52V212c0 6.627-5.373 12-12 12H76c-6.627 0-12-5.373-12-12V108c0-6.627 5.373-12 12-12h228.52c3.183 0 6.235 1.264 8.485 3.515l3.48 3.48A11.996 11.996 0 0 1 320 111.48z"/>
                            </Svg>
                            <Text style={mainStyles.buttonText}>{this.translate('save')}</Text>
                        </View>
                    </Ripple>
                </ScrollView>

                <ScrollView style={{padding: 25, backgroundColor: '#fff', margin: 25, marginTop: 0}}>
                    <Text style={mainStyles.subHeader}>{this.translate('accountmng')}</Text>

                    <Ripple
                        style={[mainStyles.button, {
                            width: 180,
                            alignItems: 'center',
                            backgroundColor: '#fcb216'
                        }]}>
                        <View style={{flexDirection: 'row'}}>
                            {/*icon: trash*/}
                            <Svg xmlns="http://www.w3.org/2000/svg" style={{width: 20, height: 20, marginRight: 10}}
                                 viewBox="0 0 448 512"><Path fill={'#fff'}
                                                             d="M192 188v216c0 6.627-5.373 12-12 12h-24c-6.627 0-12-5.373-12-12V188c0-6.627 5.373-12 12-12h24c6.627 0 12 5.373 12 12zm100-12h-24c-6.627 0-12 5.373-12 12v216c0 6.627 5.373 12 12 12h24c6.627 0 12-5.373 12-12V188c0-6.627-5.373-12-12-12zm132-96c13.255 0 24 10.745 24 24v12c0 6.627-5.373 12-12 12h-20v336c0 26.51-21.49 48-48 48H80c-26.51 0-48-21.49-48-48V128H12c-6.627 0-12-5.373-12-12v-12c0-13.255 10.745-24 24-24h74.411l34.018-56.696A48 48 0 0 1 173.589 0h100.823a48 48 0 0 1 41.16 23.304L349.589 80H424zm-269.611 0h139.223L276.16 50.913A6 6 0 0 0 271.015 48h-94.028a6 6 0 0 0-5.145 2.913L154.389 80zM368 128H80v330a6 6 0 0 0 6 6h276a6 6 0 0 0 6-6V128z"/></Svg>
                            <Text style={mainStyles.buttonText}>{this.translate('delaccount')}</Text>
                        </View>
                    </Ripple>
                </ScrollView>

                <Modal
                    isVisible={this.state.showTGUnaviableModal}
                    backdropColor={'rgba(0, 0, 0, .3)'}
                    backdropOpacity={.8}
                    backdropTransitionInTiming={1000}
                    backdropTransitionOutTiming={1000}>
                    <View style={{backgroundColor: '#fff', padding: 15, borderRadius: 5}}>
                        <Text style={mainStyles.subHeader}>{this.translate('tgunaviable')}</Text>

                        <Ripple style={[mainStyles.button, {width: 60, marginTop: 10}]}
                                onPress={() => this.setState({showTGUnaviableModal: false})}>
                            <Text style={mainStyles.buttonText}>{this.translate('close')}</Text>
                        </Ripple>
                    </View>
                </Modal>
            </ScrollView>
        );
    }
}

export default Account;