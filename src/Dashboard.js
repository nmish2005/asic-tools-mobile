import React from 'react';
import {ActivityIndicator, Picker, ScrollView, Text, TextInput, View} from 'react-native';
import {Path, Svg} from "react-native-svg";
import CheckBox from "react-native-check-box";
import Ripple from 'react-native-material-ripple';
import Modal from 'react-native-modal';
import Switch from "./_Switch";

const mainStyles = require("../assets/styles/main");

export default class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        const {appState} = props;

        this.state = {
            sortBy: 'type',
            currentDevice: {
                pool: null,
                alias: '123',
                name: 'MyAsic'
            },
            modals: {
                pool: false,
                alias: false,
                group: false,
                reboot: false,
                resave: false,
                delete: false,
                monitoring: false
            },
            displayMonitoringModal: false,
            customerDevicesData: null
        };

        this.translate = props.translateAgent;

        this.sortBy = this.sortBy.bind(this);
        this.selectField = this.selectField.bind(this);
        this.togglePools = this.togglePools.bind(this);
        this.toggleAC = this.toggleAC.bind(this);
        this.toggleGroup = this.toggleGroup.bind(this);
        this.toggleNT = this.toggleNT.bind(this);
        this.displayAliasModal = this.displayAliasModal.bind(this);
        this.displayGroupModal = this.displayGroupModal.bind(this);
        this.displayMonitoringModal = this.displayMonitoringModal.bind(this);
        this.displayPoolModal = this.displayPoolModal.bind(this);
        this.displayDeleteModal = this.displayDeleteModal.bind(this);
        this.displayRebootModal = this.displayRebootModal.bind(this);
        this.displayResaveModal = this.displayResaveModal.bind(this);
        this.editAlias = this.editAlias.bind(this);
    }

    editAlias(e, isConfirm: Boolean = false) {
        if (!isConfirm) {
            let tmp = this.state.currentDevice;

            tmp.alias = e;

            this.setState({currentDevice: tmp});
        }
        else {
            let tmp = this.state.modals;

            tmp.alias = false;

            this.setState({modals: tmp});
        }
    }

    request(url: String, ct: String, data: String, callback: Function) {
        const xhr = new XMLHttpRequest;

        xhr.open('POST', url, true);

        xhr.setRequestHeader("Content-Type", ct);

        xhr.send(data);

        xhr.onload = callback;
    }

    urlencoder(obj: Object) {
        return Object.keys(obj).map((key) => {
            return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);
        }).join('&');
    }

    displayDeleteModal(deviceID: String) {
        let tmp = this.state.modals;

        tmp.delete = true;

        this.setState({modals: tmp});
    }

    displayRebootModal(deviceID: String) {
        let tmp = this.state.modals;

        tmp.reboot = true;

        this.setState({modals: tmp});
    }

    displayResaveModal(deviceID: String) {
        let tmp = this.state.modals;

        tmp.resave = true;

        this.setState({modals: tmp});
    }

    displayPoolModal(deviceID: String) {
        let tmp = this.state.modals;

        tmp.pool = true;

        this.setState({modals: tmp});
    }

    displayAliasModal(deviceID: String) {
        let tmp = this.state.modals;

        tmp.alias = true;

        this.setState({modals: tmp});
    }

    displayMonitoringModal(deviceID: String) {
        let tmp = this.state.modals;

        tmp.monitoring = true;

        this.setState({modals: tmp});
    }

    displayGroupModal(deviceID: String) {
        let tmp = this.state.modals;

        tmp.group = true;

        this.setState({modals: tmp});
    }

    togglePools(val: Object, deviceVal: Object) {
        let tmp = this.state.customerDevicesData;
        //tmp[val][tmp.getKeyByValue(deviceVal)] = !deviceVal.poolsToggled;

        this.setState({customerDevicesData: tmp});
    }

    sortBy(type: String) {
        this.request('https://bitrex.vn.ua/ajax/getCustomerDevicesData', 'text/plain', 'groupBy=' + type, (data) => {
            let customerDevicesData = JSON.parse(data.target.responseText).data;
            Object.keys(customerDevicesData).map((val) => {
                let HRCount = 0;
                Object.keys(customerDevicesData[val]).map((deviceVal) => {
                    customerDevicesData[val][deviceVal].poolsToggled = false;
                    HRCount += customerDevicesData[val][deviceVal].cgh;
                });

                customerDevicesData[val].toggled = true;
                customerDevicesData[val].HRCount = HRCount;
            });
            this.setState({sortBy: type, customerDevicesData});
        });

        return null;
    }

    selectField(field: Array | Object, bool: Boolean) {
    }

    toggleAC(bool: Boolean, field: Object) {
        field.autoControlSettings = bool;
    }

    toggleNT(bool: Boolean, field: Object) {
        field.reportSettings = bool;
    }

    getPoolText(text) {
        const maxLength = 21;
        text = text.replace(/stratum tcp:\/\//g, '');

        if (text.length > maxLength)
            text = text.substring(0, maxLength - 1) + String.fromCharCode('8230');

        return text;
    }

    toggleGroup(field: String) {
        let tmp = this.state.customerDevicesData;
        tmp[field].toggled = !tmp[field].toggled;

        this.setState({customerDevicesData: tmp});
    }

    render() {
        return (
            <ScrollView style={{padding: 15}}>
                <View style={{flexDirection: 'row', padding: 15}}>
                    {/*Image: monitoring*/}
                    <Svg xmlns="http://www.w3.org/2000/svg"
                         style={{width: 35, height: 40, marginRight: 15}}
                         viewBox="0 0 512 512">
                        <Path fill="#000"
                              d="M500 384c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12H12c-6.6 0-12-5.4-12-12V76c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v308h436zM372.7 159.5L288 216l-85.3-113.7c-5.1-6.8-15.5-6.3-19.9 1L96 248v104h384l-89.9-187.8c-3.2-6.5-11.4-8.7-17.4-4.7z"/>
                    </Svg>
                    <Text
                        style={mainStyles.header}>{this.translate('dashboard')}</Text>
                </View>

                <View style={{alignItems: 'center', flexDirection: 'row', width: '100%'}}>
                    <Text>{this.translate('sortby')}</Text>
                    <Picker mode={'dropdown'} style={{width: 170}}
                            onValueChange={this.sortBy} selectedValue={this.state.sortBy}>
                        <Picker.Item value={'type'} label={this.translate('bytype')}/>
                        <Picker.Item value={'group'} label={this.translate('bygroup')}/>
                        <Picker.Item value={'name'} label={this.translate('byname')}/>
                        <Picker.Item value={'local_ip'} label={this.translate('bylan')}/>
                        <Picker.Item value={'ip'} label={'IP'}/>
                    </Picker>
                </View>

                {this.state.customerDevicesData === null && this.sortBy('type') &&
                <View style={{alignItems: 'center', justifyContent: 'center', margin: '10%'}}>
                    <ActivityIndicator color={'#00bcd4'} size={'large'}/>
                </View>
                }

                <View>{this.state.customerDevicesData !== null && Object.keys(this.state.customerDevicesData).map((val, index) =>
                    <View key={index}>
                        <View style={{marginTop: 15, marginBottom: 7, flexDirection: 'row'}}>
                            <Text
                                style={[mainStyles.subHeader, {marginRight: 3}]}>{val} ({this.state.customerDevicesData[val].length})</Text>

                            <Ripple onPress={() => this.toggleGroup(val)}>
                                <Svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"
                                     style={{width: 20, height: 20}}><Path
                                    fill={'#000'}
                                    d={this.state.customerDevicesData[val].toggled ? "M143 352.3L7 216.3c-9.4-9.4-9.4-24.6 0-33.9l22.6-22.6c9.4-9.4 24.6-9.4 33.9 0l96.4 96.4 96.4-96.4c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9l-136 136c-9.2 9.4-24.4 9.4-33.8 0z" : "M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"}/></Svg>
                            </Ripple>
                        </View>
                        <ScrollView alwaysBounceVertical={true} horizontal={true}>
                            <View style={mainStyles.dashboardTop}>
                                <View>
                                    <CheckBox style={mainStyles.tableItem}
                                              onClick={bool => this.selectField(val, bool)}/>
                                    {this.state.customerDevicesData[val].toggled && this.state.customerDevicesData[val].map((deviceVal, deviceIndex) =>
                                        <CheckBox
                                            style={[eval('mainStyles.dashboardInner' + deviceVal.state), mainStyles.tableItem, {
                                                height: 65,
                                                alignItems: 'center'
                                            }]}
                                            key={deviceIndex}
                                            onClick={bool => this.selectField(deviceVal, bool)}/>
                                    )}
                                </View>
                                <View>
                                    <View style={[mainStyles.tableItem, {
                                        flexDirection: 'row',
                                        height: 36,
                                        alignItems: 'center'
                                    }]}>
                                        <View>
                                            <Text>{this.translate('name')} </Text>
                                        </View>
                                        <View>
                                            <Text>{this.translate('autocontrolNotifications')}</Text>
                                        </View>
                                    </View>
                                    {this.state.customerDevicesData[val].toggled && this.state.customerDevicesData[val].map((deviceVal, deviceIndex) =>
                                        <View key={deviceIndex}
                                              style={[eval('mainStyles.dashboardInner' + deviceVal.state), mainStyles.tableItem, {
                                                  flexDirection: 'row',
                                                  justifyContent: 'space-between',
                                                  width: 320,
                                                  height: 65,
                                                  alignItems: 'center'
                                              }]}>
                                            <View style={{flexDirection: 'row'}}>
                                                <Ripple onPress={() => this.displayGroupModal('abc')}>
                                                    <Svg style={{width: 30, height: 30}}
                                                         xmlns="http://www.w3.org/2000/svg"
                                                         viewBox="0 0 512 512">
                                                        <Path fill="#337ab7"
                                                              d="M500 384c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12H12c-6.6 0-12-5.4-12-12V76c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v308h436zM372.7 159.5L288 216l-85.3-113.7c-5.1-6.8-15.5-6.3-19.9 1L96 248v104h384l-89.9-187.8c-3.2-6.5-11.4-8.7-17.4-4.7z"/>
                                                    </Svg>
                                                </Ripple>

                                                <View>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <Ripple style={{marginRight: 5}}
                                                                onPress={() => this.displayAliasModal('abc')}>
                                                            <Svg xmlns="http://www.w3.org/2000/svg"
                                                                 style={{width: 17, height: 20}}
                                                                 viewBox="0 0 576 512">
                                                                <Path fill="#337ab7"
                                                                      d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"/>
                                                            </Svg>
                                                        </Ripple>
                                                        <Text>{deviceVal.alias}</Text>
                                                        <Text onPress={() => this.togglePools()}
                                                              style={[mainStyles.link, {marginLeft: 5}]}>{this.translate('more')} &raquo;</Text>
                                                    </View>
                                                    <View>
                                                        <Text
                                                            style={mainStyles.unimportant}>{this.getPoolText(deviceVal.pool[0].url)}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                                                <Switch style={{marginRight: 3}}
                                                        onToggle={(bool) => this.toggleNT(bool, deviceVal)}
                                                        isToggled={deviceVal.reportSettings}/>

                                                <Switch onToggle={(bool) => this.toggleAC(bool, deviceVal)}
                                                        isToggled={deviceVal.autoControlSettings}/>
                                            </View>
                                        </View>
                                    )}
                                </View>
                                <View>
                                    <View style={[mainStyles.tableItem, {
                                        height: 36,
                                        justifyContent: 'flex-start',
                                        alignItems: 'center'
                                    }]}>
                                        <Text>{this.state.customerDevicesData[val].HRCount} (GH/S)</Text>
                                    </View>
                                    {this.state.customerDevicesData[val].toggled && this.state.customerDevicesData[val].map((deviceVal, deviceIndex) =>
                                        <View key={deviceIndex}
                                              style={[eval('mainStyles.dashboardInner' + deviceVal.state), mainStyles.tableItem, {
                                                  width: 125,
                                                  height: 65,
                                                  alignItems: 'center'
                                              }]}>
                                            <View style={{flexDirection: 'row'}}><Text
                                                style={mainStyles.bold}>Cur </Text><Text
                                                style={eval(deviceVal.cgh < deviceVal.agh ? 'mainStyles.important' : 'mainStyles.text')}>{deviceVal.cgh}</Text></View>
                                            <View style={{flexDirection: 'row'}}><Text
                                                style={mainStyles.bold}>Avg </Text><Text
                                                style={mainStyles.unimportant}>{deviceVal.agh}</Text></View>
                                        </View>
                                    )}
                                </View>
                                <View>
                                    <View style={[mainStyles.tableItem, {
                                        height: 36,
                                        justifyContent: 'flex-start',
                                        alignItems: 'center'
                                    }]}>
                                        <Text>TEMP/<Text
                                            style={{color: '#808082'}}>{this.translate('uptime')}</Text></Text>
                                    </View>
                                    {this.state.customerDevicesData[val].toggled && this.state.customerDevicesData[val].map((deviceVal, deviceIndex) =>
                                        <View key={deviceIndex}
                                              style={[eval('mainStyles.dashboardInner' + deviceVal.state), mainStyles.tableItem, {
                                                  width: 157,
                                                  height: 65,
                                                  alignItems: 'center'
                                              }]}>
                                            <Text>
                                                {deviceVal.temp} &deg;C
                                            </Text>
                                            <Text style={mainStyles.unimportant}>
                                                {deviceVal.uptime}
                                            </Text>
                                        </View>
                                    )}
                                </View>
                                <View>
                                    <View style={[mainStyles.tableItem, {
                                        height: 36,
                                        justifyContent: 'flex-start',
                                        alignItems: 'center'
                                    }]}>
                                        <Text>Lan IP/<Text style={{color: '#808082'}}>IP</Text></Text>
                                    </View>

                                    {this.state.customerDevicesData[val].toggled && this.state.customerDevicesData[val].map((deviceVal, deviceIndex) =>
                                        <View key={deviceIndex}
                                              style={[eval('mainStyles.dashboardInner' + deviceVal.state), mainStyles.tableItem, {
                                                  width: 120,
                                                  height: 65,
                                                  alignItems: 'center'
                                              }]}>
                                            <Text>
                                                {deviceVal.ip}
                                            </Text>
                                            <Text style={mainStyles.unimportant}>
                                                {deviceVal.local_ip}
                                            </Text>
                                        </View>
                                    )}
                                </View>
                                <View>
                                    <View style={[mainStyles.tableItem, {
                                        height: 36,
                                        justifyContent: 'flex-start',
                                        alignItems: 'center'
                                    }]}>
                                        <Text>{this.translate('group')}</Text>
                                    </View>

                                    {this.state.customerDevicesData[val].toggled && this.state.customerDevicesData[val].map((deviceVal, deviceIndex) =>
                                        <View key={deviceIndex}
                                              style={[eval('mainStyles.dashboardInner' + deviceVal.state), mainStyles.tableItem, {
                                                  width: 120,
                                                  height: 65,
                                                  alignItems: 'center',
                                                  flexDirection: 'row',
                                                  justifyContent: 'center'
                                              }]}>
                                            <Text>
                                                {deviceVal.group}
                                            </Text>

                                            <Ripple onPress={() => this.displayGroupModal('abc')}>
                                                <Svg xmlns="http://www.w3.org/2000/svg"
                                                     style={{width: 17, height: 20, marginLeft: 5}}
                                                     viewBox="0 0 576 512">
                                                    <Path fill="#337ab7"
                                                          d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"/>
                                                </Svg>
                                            </Ripple>
                                        </View>
                                    )}
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                )}</View>

                <Modal
                    isVisible={this.state.displayMonitoringModal}
                    backdropColor={'rgba(0, 0, 0, .3)'}
                    backdropOpacity={.8}
                    backdropTransitionInTiming={1000}
                    backdropTransitionOutTiming={1000}>
                    <View style={{backgroundColor: '#fbfbfb', padding: 15, borderRadius: 5}}>
                        <Text style={mainStyles.subHeader}>{this.translate('tgunaviable')}</Text>

                        <Ripple style={[mainStyles.button, {width: 60, marginTop: 10}]}
                                onPress={() => this.setState({displayMonitoringModal: false})}>
                            <Text style={mainStyles.buttonText}>{this.translate('close')}</Text>
                        </Ripple>
                    </View>
                </Modal>

                <Modal
                    isVisible={this.state.displayGroupModal}
                    backdropColor={'rgba(0, 0, 0, .3)'}
                    backdropOpacity={.8}
                    backdropTransitionInTiming={1000}
                    backdropTransitionOutTiming={1000}>
                    <View style={{backgroundColor: '#fbfbfb', padding: 15, borderRadius: 5}}>
                        <Text style={mainStyles.subHeader}>{this.translate('tgunaviable')}</Text>

                        <Ripple style={[mainStyles.button, {width: 60, marginTop: 10}]}
                                onPress={() => this.setState({displayGroupModal: false})}>
                            <Text style={mainStyles.buttonText}>{this.translate('close')}</Text>
                        </Ripple>
                    </View>
                </Modal>

                <Modal
                    isVisible={this.state.modals.alias}
                    backdropColor={'rgba(0, 0, 0, .3)'}
                    backdropOpacity={.8}
                    backdropTransitionInTiming={1000}
                    backdropTransitionOutTiming={1000}>
                    <View style={{backgroundColor: '#fbfbfb', borderRadius: 5}}>
                        <View style={mainStyles.modalHeading}>
                            <Text
                                style={mainStyles.subHeader}>{this.translate('nameEditing')} {this.state.currentDevice.name} {this.state.currentDevice.alias}</Text>
                            <Ripple onPress={() => {
                                let tmp = this.state.modals;
                                tmp.alias = false;
                                this.setState({modals: tmp})
                            }}>
                                <Text style={[mainStyles.unimportant, {fontSize: 22}]}>&times;</Text>
                            </Ripple>
                        </View>

                        <View style={mainStyles.modalContent}>
                            <TextInput underlineColorAndroid={'transparent'} value={this.state.currentDevice.alias}
                                       onChangeText={this.editAlias} style={{
                                backgroundColor: '#fff',
                                marginBottom: 7,
                                borderWidth: 1,
                                borderColor: '#ccc',
                                padding: 3
                            }}/>

                            <View style={{flexDirection: 'row'}}>
                                <Svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"
                                     style={{width: 15, height: 15}}>
                                    <Path fill="#000"
                                          d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"/>
                                </Svg>

                                <Text style={{marginLeft: 3}}>
                                    {this.translate('changeWorkerNameP1')}
                                    <Text style={mainStyles.bold}>
                                        [worker].[name]
                                    </Text>
                                    {this.translate('changeWorkerNameP2')}
                                </Text>
                            </View>
                        </View>

                        <View style={mainStyles.modalFooter}>
                            <Ripple onPress={() => this.editAlias(null, true)}
                                    style={[mainStyles.button, {width: 105}]}>
                                <Text style={mainStyles.buttonText}>{this.translate('save')}</Text>
                            </Ripple>
                        </View>
                    </View>
                </Modal>

                <Modal
                    isVisible={this.state.displayPoolModal}
                    backdropColor={'rgba(0, 0, 0, .3)'}
                    backdropOpacity={.8}
                    backdropTransitionInTiming={1000}
                    backdropTransitionOutTiming={1000}>
                    <View style={{backgroundColor: '#fbfbfb', padding: 15, borderRadius: 5}}>
                        <Text style={mainStyles.subHeader}>{this.translate('tgunaviable')}</Text>

                        <Ripple style={[mainStyles.button, {width: 60, marginTop: 10}]}
                                onPress={() => this.setState({displayPoolModal: false})}>
                            <Text style={mainStyles.buttonText}>{this.translate('close')}</Text>
                        </Ripple>
                    </View>
                </Modal>

                <Modal
                    isVisible={this.state.displayRebootModal}
                    backdropColor={'rgba(0, 0, 0, .3)'}
                    backdropOpacity={.8}
                    backdropTransitionInTiming={1000}
                    backdropTransitionOutTiming={1000}>
                    <View style={{backgroundColor: '#fbfbfb', padding: 15, borderRadius: 5}}>
                        <Text style={mainStyles.subHeader}>{this.translate('tgunaviable')}</Text>

                        <Ripple style={[mainStyles.button, {width: 60, marginTop: 10}]}
                                onPress={() => this.setState({displayMonitoringModal: false})}>
                            <Text style={mainStyles.buttonText}>{this.translate('close')}</Text>
                        </Ripple>
                    </View>
                </Modal>

                <Modal
                    isVisible={this.state.displayResaveModal}
                    backdropColor={'rgba(0, 0, 0, .3)'}
                    backdropOpacity={.8}
                    backdropTransitionInTiming={1000}
                    backdropTransitionOutTiming={1000}>
                    <View style={{backgroundColor: '#fbfbfb', padding: 15, borderRadius: 5}}>
                        <Text style={mainStyles.subHeader}>{this.translate('tgunaviable')}</Text>

                        <Ripple style={[mainStyles.button, {width: 60, marginTop: 10}]}
                                onPress={() => this.setState({displayGroupModal: false})}>
                            <Text style={mainStyles.buttonText}>{this.translate('close')}</Text>
                        </Ripple>
                    </View>
                </Modal>

                <Modal
                    isVisible={this.state.displayDeleteModal}
                    backdropColor={'rgba(0, 0, 0, .3)'}
                    backdropOpacity={.8}
                    backdropTransitionInTiming={1000}
                    backdropTransitionOutTiming={1000}>
                    <View style={{backgroundColor: '#fbfbfb', padding: 15, borderRadius: 5}}>
                        <Text style={mainStyles.subHeader}>{this.translate('tgunaviable')}</Text>

                        <Ripple style={[mainStyles.button, {width: 60, marginTop: 10}]}
                                onPress={() => this.setState({displayAliasModal: false})}>
                            <Text style={mainStyles.buttonText}>{this.translate('close')}</Text>
                        </Ripple>
                    </View>
                </Modal>

            </ScrollView>
        );
    }
}