import React from 'react';
import {Text, TextInput, TouchableOpacity, View} from "react-native";
import mainStyles from '../assets/styles/main';
import Modal from 'react-native-modal';

export default class LoginForm extends React.Component {
    constructor(props) {
        super(props);

        this.translate = props.translateAgent;
        this.appState = props.appState;

        this.state = {
            isLogin: true,
            formData: {
                email: '',
                pass: ''
            }
        };

        this.toggleLogin = this.toggleLogin.bind(this);
        this.handleAuth = this.handleAuth.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePassChange = this.handlePassChange.bind(this);
        this.forgotPass = this.forgotPass.bind(this);
    }

    request(url: String, ct: String, data: String, callback: Function) {
        const xhr = new XMLHttpRequest;

        xhr.open('POST', url, true);

        xhr.setRequestHeader("Content-Type", ct);

        xhr.send(data);

        xhr.onload = callback;
    }

    validateFD() {
        return {email: true, pass: true};
    }

    forgotPass() {

    }

    handleAuth() {
        if (this.state.isLogin) {
            const serializedData = 'login=' + this.state.formData.email.replace(/@/g, '%40') + '&pass=' + this.state.formData.pass + '&lang=' + this.appState.locale.toLowerCase();

            this.request('https://bitrex.vn.ua/ajax/signin', 'text/plain', serializedData, () => {
                return this.props.onAuthChange();
            });
        }
        else {
            const isValid = this.validateFD();

            if (!isValid.email)
                return console.warn('invalid email');

            if (!isValid.pass)
                return console.warn('invalid pass');

            const serializedData = 'login=' + this.state.formData.email.replace(/@/g, '%40') + '&pass=' + this.state.formData.pass;

            this.request('https://bitrex.vn.ua/ajax/signup', 'text/plain', serializedData, () => {
                return this.props.onAuthChange();
            });
        }
    }

    handlePassChange(e) {
        let formData = this.state.formData;
        formData.pass = e;

        this.setState({formData});
    }

    handleEmailChange(e) {
        let formData = this.state.formData;
        formData.email = e;

        this.setState({formData});
    }

    toggleLogin() {
        this.setState({isLogin: !this.state.isLogin});
    }

    render() {
        return (<View>
                <Text style={{width: '100%', padding: 25, fontSize: 15}}>{this.translate('authtosee')}</Text>

                <Modal
                    backdropColor={'rgba(0, 0, 0, .3)'}
                    backdropOpacity={.8}
                    style={{zIndex: 9999999}}
                    animationIn={'slideInUp'}
                    animatioOut={'slideOutUp'}
                    animationInTiming={1000}
                    animationOutTiming={700}
                    backdropTransitionInTiming={1000}
                    backdropTransitionOutTiming={1000}>
                    <View style={{padding: 15, backgroundColor: '#fff'}}>
                        <Text
                            style={[mainStyles.header, {textAlign: 'center'}]}>{this.state.isLogin ? this.translate('auth') : this.translate('register')}</Text>
                        <TextInput value={this.state.formData.email} onChangeText={this.handleEmailChange} style={{
                            width: '100%',
                            height: 30,
                            borderColor: '#d9d9d9',
                            borderWidth: 1.5,
                            marginTop: 30,
                            paddingLeft: 5
                        }} placeholder={this.translate('email')} underlineColorAndroid={'transparent'}/>
                        <TextInput value={this.state.formData.pass} secureTextEntry={true}
                                   onChangeText={this.handlePassChange} style={{
                            width: '100%',
                            height: 30,
                            borderColor: '#d9d9d9',
                            borderWidth: 1.5,
                            marginTop: 15,
                            paddingLeft: 5
                        }} placeholder={this.translate('pass')} underlineColorAndroid={'transparent'}/>
                        <TouchableOpacity onPress={this.handleAuth}
                                          style={[mainStyles.button, {marginTop: 20, width: 160}]}>
                            <Text
                                style={[mainStyles.buttonText, {textAlign: 'center'}]}>{this.state.isLogin ? this.translate('authbutton') : this.translate('registerbutton')}</Text>
                        </TouchableOpacity>

                        <View style={{flexDirection: 'row'}}>
                            <Text onPress={this.props.changeLocale}
                                  style={{color: 'grey', margin: 3}}>{this.appState.nextLocale}</Text>

                            <Text>&bull;</Text>

                            <Text onPress={this.toggleLogin} style={{
                                color: 'grey',
                                margin: 3
                            }}>{this.state.isLogin ? this.translate('donthaveaccount') : this.translate('alreadyhaveaccount')}</Text>

                            {this.state.isLogin && [
                                <Text key={0}>&bull;</Text>,
                                <Text key={1} onPress={this.forgotPass}
                                      style={{color: 'grey', margin: 3}}>{this.translate('passforgot')}</Text>
                            ]}
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}