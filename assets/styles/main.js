const mainStyles = {
    MainView: {
        backgroundColor: '#f4f7f9',
        width: '100%',
        height: '100%',
    },
    link: {
        fontWeight: '600',
        color: '#337ab7'
    },
    modal: {
        backgroundColor: '#fff',
        margin: 15,
        padding: 15,
        alignSelf: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: '-1000%',
        zIndex: 50
    },
    blueNav: {
        backgroundColor: '#00bcd4',
        position: 'absolute',
        left: 0,
        padding: 15,
        width: '100%',
        zIndex: 15
    },
    hamburger: {
        width: '100%',
        padding: 12,
        paddingTop: 30,
    },
    important: {
        color: 'crimson',
        fontWeight: '300'
    },
    mainNav: {
        borderTopColor: '#ededed',
        borderTopWidth: 3,
        flexDirection: 'row',
        borderStyle: 'solid',
        zIndex: 15,
        backgroundColor: '#fff'
    },
    navbarBrand: {
        fontSize: 25,
        fontWeight: "900",
        fontFamily: 'sans-serif',
        padding: 7,
        color: '#000',
        width: '70%'
    },
    navButtonLocale: {
        borderRadius: 5,
        padding: 5,
        width: 50,
        backgroundColor: '#00bcd4',
    },
    navButton: {
        borderRadius: 5,
        padding: 10,
        width: 70,
        backgroundColor: '#00bcd4',
    },
    button: {
        borderRadius: 5,
        padding: 10,
        backgroundColor: '#00bcd4',
    },
    buttonText: {
        color: '#fff',
        fontWeight: '700'
    },
    navLink: {
        borderRadius: 5,
        padding: 10,
        width: 70,
        backgroundColor: 'transparent',
    },
    footer: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 15
    },
    footerText: {
        fontSize: 17,
        color: '#555',
        textAlign: 'center'
    },
    header: {
        fontSize: 20,
        fontWeight: "700"
    },
    subHeader: {
        fontSize: 17,
        fontWeight: "600"
    },
    bold: {
        fontWeight: "600"
    },
    dashboardTop: {
        backgroundColor: '#ddd',
        flexDirection: 'row'
    },
    dashboardInnernorm: {
        backgroundColor: 'whitesmoke'
    },
    dashboardInnerdead: {
        backgroundColor: '#ff9a66'
    },
    dashboardInnerwarn: {
        backgroundColor: '#fff3cd'
    },
    dashboardFooter: {
        backgroundColor: '#fff'
    },
    tableItem: {
        borderColor: '#ccc',
        borderWidth: 1,
        padding: 5
    },
    unimportant: {
        color: 'grey',
        fontWeight: '300'
    },
    text: {
        color: '#000',
        fontWeight: '300'
    },
    modalHeading: {
        padding: 7,
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderRadius: 5
    },
    modalContent: {
        padding: 12
    },
    modalFooter: {
        ...this.modalHeading,
        justifyContent: 'flex-end',
        padding: 5
    }
};

module.exports = mainStyles;

export default mainStyles;