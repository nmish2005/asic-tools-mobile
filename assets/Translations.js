const Translations = {
    dashboard: {
        RU: 'Мониторинг',
        EN: 'Dashboard'
    },
    account: {
        RU: 'Аккаунт',
        EN: 'Account'
    },
    logout: {
        RU: 'Выйти',
        EN: 'Log out'
    },
    signin: {
        RU: 'Войти',
        EN: 'Sign in'
    },
    signup: {
        RU: 'Зарегистрироваться',
        EN: 'Sign up'
    },
    close: {
        RU: 'Закрыть',
        EN: 'Close'
    },
    contactediting: {
        RU: 'Редактирование контактных данных:',
        EN: 'Contact data:'
    },
    yourName: {
        RU: 'Ваше имя',
        EN: 'Your name'
    },
    oldpass: {
        RU: 'Старый пароль',
        EN: 'Old password'
    },
    newpass: {
        RU: 'Новый пароль',
        EN: 'New password'
    },
    pass: {
        RU: 'Ваш пароль',
        EN: 'Your password'
    },
    new2pass: {
        RU: 'Новый пароль еще раз',
        EN: 'New password once more'
    },
    fballowed: {
        RU: 'Вход возможен используя',
        EN: 'Access allowed using'
    },
    email: {
        RU: 'Ваш email',
        EN: 'Your email'
    },
    save: {
        RU: 'СОХРАНИТЬ',
        EN: 'SAVE'
    },
    notifications: {
        RU: 'Уведомления:',
        EN: 'Notifications:'
    },
    accountmng: {
        RU: 'Управление аккаунтом:',
        EN: 'Manage account:'
    },
    delaccount: {
        RU: 'УДАЛИТЬ АККАУНТ',
        EN: 'DELETE ACCOUNT'
    },
    canreceive: {
        RU: 'Вы можете получать уведомления о работе устройств в Telegram мессенджер.',
        EN: 'You can receive notifications about the operation of devices in Telegram messenger.'
    },
    botinstructions: {
        RU: 'Чтобы подключить свой Telegram аккаунт необходимо добавить бот ',
        EN: 'To connect your Telegram account you need to add a bot '
    },
    botinstructionsp2: {
        RU: ' и следуйте его инструкциям.',
        EN: ' and follow its instructions.'
    },
    tgallowed: {
        RU: 'Разрешено подключение нескольких аккаунтов Telegram.',
        EN: 'It is allowed to connect several Telegram accounts.'
    },
    notificationslang: {
        RU: 'Язык уведомлений',
        EN: 'Language of notifications'
    },
    notifyme: {
        RU: 'Если устройство не присылает отчеты - уведомлять меня',
        EN: 'If the device doesn\'t send report - notify me'
    },
    receiveemail: {
        RU: 'Получать Email уведомления для ',
        EN: 'Receive Email notifications for '
    },
    receivetg: {
        RU: 'Получать Telegram уведомления для ',
        EN: 'Receive Telegram notifications for '
    },
    tgunaviable: {
        RU: 'Ссылки не поддерживаются Вашим девайсом или у Вас не установлен Telegram',
        EN: 'Links is not supported by your device or you don\'t have Telegram app'
    },
    onetime: {
        RU: '1 раз',
        EN: 'once'
    },
    everyhour: {
        RU: 'каждый час',
        EN: 'every hour'
    },
    every3hours: {
        RU: 'каждых 3 часа',
        EN: 'every 3 hours'
    },
    onceaday: {
        RU: '1 раз в день',
        EN: 'once a day'
    },
    silentmode: {
        RU: 'Тихий режим',
        EN: 'Silent mode'
    },
    sortby: {
        RU: 'Сортировать по',
        EN: 'Group by'
    },
    bytype: {
        RU: 'типу',
        EN: 'type'
    },
    bygroup: {
        RU: 'группам',
        EN: 'groups'
    },
    byname: {
        RU: 'названию',
        EN: 'name'
    },
    bylan: {
        RU: 'локальной сети',
        EN: 'network'
    },
    auth: {
        RU: 'Вход в личный кабинет',
        EN: 'Login to your account'
    },
    register: {
        RU: 'Завести аккаунт',
        EN: 'Create new account'
    },
    authbutton: {
        RU: 'Войти',
        EN: 'Sign in'
    },
    registerbutton: {
        RU: 'Зарегистрироваться',
        EN: 'Sign up'
    },
    authtosee: {
        RU: 'Для просмотра страницы нужно выполнить вход.',
        EN: 'Please, sign in first to see this page.'
    },
    donthaveaccount: {
        RU: 'Ещё не имеете аккаунта?',
        EN: 'Do not have an account?'
    },
    alreadyhaveaccount: {
        RU: 'Уже имеете аккаунт?',
        EN: 'Already have an account?'
    },
    passforgot: {
        RU: 'Забыли пароль?',
        EN: 'Forgot password?'
    },
    name: {
        RU: 'НАЗВАНИЕ',
        EN: 'NAME'
    },
    autocontrolNotifications: {
        RU: 'АВТОКОНТРОЛЬ / УВЕДОМЛЕНИЯ',
        EN: 'AUTOCONTROL / NOTIFICATIONS'
    },
    uptime: {
        RU: 'ВРЕМЯ СТАРТА',
        EN: 'UPTIME'
    },
    group: {
        RU: 'ГРУППА',
        EN: 'GROUP'
    },
    more: {
        RU: 'Больше',
        EN: 'More'
    },
    changeWorkerNameP1: {
        RU: 'Изменение поля "Имя" повлечет пересохранение пулов устройства с названиями воркеров ',
        EN: 'Changing "Name" field will do the resave of device pools with worker names '
    },
    changeWorkerNameP2: {
        RU: ' даже если настройка "Разрешить автоматические действия" отключена.',
        EN: ' even if "Allow automatic actions" setting disabled.'
    },
    nameEditing: {
        RU: 'Редактирование имени ',
        EN: 'Editing name of '
    }
};

export default Translations;